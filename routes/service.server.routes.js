'use strict';
////////////////////////////////////////////////////////////////////////////////
//	Routes for handling API service requests:
//	Intercepts the  service routes and calls the
//	controller "service.server.controller" for authentication.
////////////////////////////////////////////////////////////////////////////////
var cacheService      = require('../services/cache.server.service.js'),
	servicecontroller = require('../controllers/service.server.controller.js'),
	logger            = require('../utils/logger.server.util.js');


module.exports = function(app) {

	// Inspect all authenticated service endpoints for valid ticket
	app.use('/pctserver/api/services/:module', function(req, res, next) {
		logger.info(__filename, "- Vaidating service ticket: ", req.query.ticket);
		// Get ticket from request query params
		var tempStore  = { "ticket" : req.query.ticket };
		if (!tempStore.ticket) {
			res.status(403).send('');
			return;
		}
		// retrieve userprincipal from cache
		cacheService.get(tempStore.ticket, function(err, userprincipal) {
			if (err) {
				// error retrieving userprincipal from cache
				res.status(403).send('');
				return;				
			}			
			if (userprincipal) {
				logger.info(__filename, "- cached object: ", userprincipal);
				logger.info(__filename, "- cached user : ", userprincipal.username);
				// attach user details to the request itself for service processing
				req.params["username"] = userprincipal.username;
				req.params["userprincipal"] = userprincipal;
				next()
			} else {
				logger.error(__filename, "- url pattern not found. ");
				res.status(403).send('');
				return;					
			}
		});
	});


	// Authenticated service routes
	app.get('/pctserver/api/services/:module', servicecontroller.processGetRequest);

	// Public service routes
	app.get('/pctserver/api/public/services/:module', servicecontroller.processPublicGetRequest);

}